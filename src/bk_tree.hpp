#pragma once

#include "IDictionary.hpp"
#include "tools.hpp"

#include <memory>
#include <climits>
#include <vector>

#include "tbb/tbb.h"

class BK_Tree : public IDictionary
{
  class Node
  {
  public:
    Node(const std::string word)
      : word_(word)
    {}

    Node(Node&& node)
      : word_(node.getWord())
      , deleted_(node.isDeleted())
    {}

    void setDeleted(bool deleted) { deleted_.store(deleted); }
    bool isDeleted() const { return deleted_.load(); }

    const std::string getWord() const { return word_; }

    std::shared_ptr<Node> findChild(const int distance) const
    {
      for (auto& i : children_)
      {
        if (i.first == distance)
          return i.second;
      }
      return nullptr;
    }

    std::shared_ptr<Node> operator[](int distance) const { return findChild(distance); }
    tbb::concurrent_hash_map<int, std::shared_ptr<Node> > getChildren() const { return children_; }

    void addChild(const std::string& word)
    {
      int distance = levenshtein(word, word_);
      for (auto& i : children_)
      {
        if (i.first == distance)
        {
          i.second->addChild(word);
          return;
        }
      }
      children_.insert(std::pair<int, std::shared_ptr<Node> >(distance, std::make_shared<Node>(Node(word))));
    }

  private:
    const std::string word_;
    tbb::concurrent_hash_map<int, std::shared_ptr<Node> > children_;

    std::atomic<bool> deleted_ = false;
  };

  void search_aux(tbb::concurrent_unordered_set<result_t>& ret, std::shared_ptr<Node> node, const std::string word, const int tolerance) const
  {
    const int current_distance = levenshtein(node->getWord(), word);
    const int minDist = current_distance - tolerance;
    const int maxDist = current_distance + tolerance;

    if (node->isDeleted() == false && current_distance <= tolerance)
    {
      ret.insert(result_t(node->getWord(), current_distance));
    }
  
    auto node_map = node->getChildren();

    tbb::parallel_for_each(node_map.begin(), node_map.end(), [&] (auto elem) {
      if (elem.first >= minDist && elem.first <= maxDist)
      {
        std::shared_ptr<Node> tmp_node = elem.second;
        if (tmp_node)
          search_aux(ret, tmp_node, word, tolerance);
      }
    });
  }

public:
  result_t search(const std::string& w) const
  {
    if (!root)
      return std::pair<std::string, int>("", w.size());
    auto helper = [] (tbb::concurrent_unordered_set<result_t>& list) {
      std::pair<std::string, int> minimum("", INT_MAX);
      for (auto& w : list)
      {
        if (minimum.second > w.second)
        {
          minimum = w;
        }
      }
      return minimum;
    };

    tbb::concurrent_unordered_set<result_t> result;

    search_aux(result, root, w, radius);
    if (result.size())
      return helper(result);

    result = tbb::concurrent_unordered_set<result_t>();

    search_aux(result, root, w, INT_MAX);
    if (result.size())
      return helper(result);

    // Shouldn't happen in any case
    return std::pair<std::string, int>("", w.size());
  }

  BK_Tree() = default;

  BK_Tree(const std::initializer_list<std::string>& init)
  {
    for (auto& s : init)
      insert(s);
  }

  template <class Iterator>
  BK_Tree(Iterator begin, Iterator end)
  {
    for (auto s = begin; s != end; s++)
      insert(*s);
  }
  
  void init(const std::vector<std::string>& word_list)
  {
    for (auto& i : word_list)
      insert(i);
  }

  void insert(const std::string& w)
  {
    if (!root)
      root = std::make_shared<Node>(Node(w));
    else
      root->addChild(w);
  }
  
  void erase(const std::string& w)
  {
    auto tmp_node = root;
    int distance;
    while ((distance = levenshtein(w, tmp_node->getWord())))
    {
      auto tmp_inner_node = tmp_node->findChild(distance);
      if (!tmp_inner_node)
        return ;
      tmp_node = tmp_inner_node;
    }
    tmp_node->setDeleted(true);
  }

private:
  std::shared_ptr<Node> root;

  const int radius = 3;
};